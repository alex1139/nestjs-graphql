import { registerAs } from '@nestjs/config';

export default registerAs('postgres', () => ({
    POSTGRES_DB_HOST: process.env.POSTGRES_DB_HOST,
    POSTGRES_DB_PORT: process.env.POSTGRES_DB_PORT,
    POSTGRES_USER: process.env.POSTGRES_USER,
    POSTGRES_PASSWORD: process.env.POSTGRES_PASSWORD,
    POSTGRES_DB_SCHEMA:
        process.env.POSTGRES_DB_PREFIX + '_' + process.env.POSTGRES_DB_NAME
}));