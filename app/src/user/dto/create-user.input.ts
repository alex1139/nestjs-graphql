import { InputType, Int, Field } from '@nestjs/graphql';
import {IsEmail, IsNotEmpty, IsOptional, MinLength} from "class-validator";

@InputType()
export class CreateUserInput {
  @IsEmail()
  @IsNotEmpty()
  @Field(() => String)
  email: string;

  @MinLength(6)
  @IsNotEmpty()
  @Field(() => String)
  password: string;

  @Field({ nullable: true })
  @IsOptional()
  role: string;

}
