import { ObjectType, Field, Int } from '@nestjs/graphql';
import { MinLength, IsNotEmpty } from 'class-validator';
import {Column, Entity, PrimaryGeneratedColumn, BaseEntity, BeforeInsert, CreateDateColumn} from "typeorm";
import {ROLE_USER} from "../user.service";
import * as bcrypt from 'bcryptjs';

@ObjectType()
@Entity()
export class User extends BaseEntity{
  @PrimaryGeneratedColumn()
  @Field(() => Int)
  id: number;

  @Column({ unique: true })
  @Field()
  @IsNotEmpty()
  email: string;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @Field()
  @IsNotEmpty()
  @MinLength(6)
  password: string;

  @Column()
  @Field(()=>String, {defaultValue:ROLE_USER})
  role?: string;

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 8);
  }

  async validatePassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password);
  }
}
