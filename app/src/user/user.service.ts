import { Injectable } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import {User} from "./entities/user.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {DeleteResult, Repository} from "typeorm";

export const ROLE_USER = "role_user";
export const ROLE_ADMIN = "role_admin";
export const ROLE_MANAGER = "role_manager";
export const ROLE_LIST = [ROLE_USER, ROLE_ADMIN, ROLE_MANAGER];

@Injectable()
export class UserService {

  constructor(@InjectRepository(User) private readonly userRepository:Repository<User>) {  }

  async create(createUserInput: CreateUserInput) {
    const user = await this.userRepository.create(createUserInput);
    await user.save();

    delete user.password;
    return user;
  }

  findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  findOne(id: number) {
    return this.userRepository.findOne(id);
  }

  update(id: number, updateUserInput: UpdateUserInput) {
    return `This action updates a #${id} user`;
  }

  async remove(id: number): Promise<DeleteResult> {
    return this.userRepository.delete(id);
  }
}
